Version 1.3.2
=============
- Fix shaping of യ്തു/y1th1u1 in InDesign

Version 1.3.1
=============
- Fix name in fontconfig

Version 1.3
===========
- Support old shapers from Windwos XP, old Pango, Lipika (Adobe) to HarfBuzz, Uniscribe

Version 1.2
===========
- Add RIT prefix to font name

Version 1.1
===========
- SFD: Encoding corrections so that uni2215 and uni25CC encodings are corrected
- [FIX] glyph name of ഷ്ര, and add missing shaping rule for it. Thanks to test script for identifying it.
- Add test script for checking glyphs with Unicode codepoint or shaping rule
- Gitlab CI: add packages for OTF building
- Add fontconfig file (and move Appstream metadata to sub directory)
- Build script: shrink OTF file size even further

Version 1.0.1
=============
- Fixed two glyph shapes for ഞു & ഞൂ

Version 1.0
===========
- First version of Kutty font designed by artist Kutty Kondungallur
